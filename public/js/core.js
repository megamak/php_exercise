var core = function () {
    jQuery(document).ready(function ($) {
        $.fn.dataTable.moment( 'D-MMM-YY' );
        //initialize all datepicker inputs
        $( ".datepicker" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });

        $('#results').DataTable({
            "sDom": '<"H"lr>t<"F"ip>'
        });


        //parse the data in a way that can be used by ChartJS
        var datapoints = [];
        datapoints[0] = _.map(window.graphData, function(item) { return {
            x: new Date(item.date),
            y: item.open
        }});
        datapoints[1] = _.map(window.graphData, function(item) { return {
            x: new Date(item.date),
            y: item.close
        }});

        //if we have data, proceed with creating the table the graph
        if (! _.isEqual(window.graphData, {}) ) {

            var canvasJSoptions = {
                title: {
                    text: "Data for " +
                    $("input[name='company']").val() +
                    ', from ' +
                    _.last(window.graphData).date +
                    ' to ' +
                    _.first(window.graphData).date
                },
                animationEnabled: true,
                data: [
                    {
                        type: "spline",
                        legendText:"Open values",
                        showInLegend: true,
                        dataPoints: datapoints[0]
                    },{
                        type: "spline",
                        legendText:"Close values",
                        showInLegend: true,
                        dataPoints: datapoints[1]
                    }
                ]
            };

            $("#chartContainer").CanvasJSChart(canvasJSoptions);
        }
    });
}();
