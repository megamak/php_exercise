<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Exercise v16.0.0 - PHP & jQuery</title>

    <!-- Fonts -->
    <script src="//code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.15/sorting/datetime-moment.js"></script>
    <script src="{{URL::asset('js/jquery.canvasjs.min.js')}}"></script>
    <script src="{{URL::asset('js/core.js?5')}}"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          type="text/css">
    <link href="{{URL::asset('css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<script>
    window.graphData = <?php echo isset($graphData) ? json_encode($graphData) : "{}"; ?>;
</script>
<body>
<hr/>
<div class="container">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            {{ Form::open(['url' => '/submit', 'class'=> 'form-horizontal']) }}
            <div class="form-group required">
                {{Form::label('company','Company Name', ['class' =>'col-sm-4 control-label']) }}
                <div class="col-sm-8">
                    {{Form::text('company', '', array(
                    'required'=>'required',
                    'class'=> 'form-control'))}}
                </div>
            </div>
            <div class="form-group required">
                {{Form::label('startDate','Start Date', ['class' =>'col-sm-4 control-label']) }}
                <div class="col-sm-8">
                    {{Form::text('startDate', '', array(
                    'class' => 'datepicker form-control',
                    'required'=> 'required'))}}
                </div>
            </div>

            <div class="form-group required">
                {{Form::label('endDate','End Date', ['class' =>'col-sm-4 control-label']) }}
                <div class="col-sm-8">
                    {{Form::text('endDate', '', array(
                                'class' => 'datepicker form-control',
                                'required'=> 'required'))}}
                </div>
            </div>

            <div class="form-group required">
                {{Form::label('email','Email', ['class' =>'col-sm-4 control-label']) }}
                <div class="col-sm-8">
                    {{Form::text('email','',['required'=> 'required', 'class'=>'form-control'])}}
                </div>
            </div>


            <div class="form-group">
                <div class="col-xs-3 col-xs-offset-5">
                    {{Form::submit('Search', ['class' => 'btn btn-primary btn-block btn-default'])}}
                </div>
            </div>
            {{ Form::close() }}

        </div>
    </div>

    @if ( isset($companyData))
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <fieldset>
                    <legend>Results</legend>

                    <table id="results" class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            @foreach( $companyData[0] as $row )
                                <th>{{$row}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( array_slice($companyData, 1) as $row )
                            <tr>
                                @foreach($row as $column)
                                    <td>{{$column}}</td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </fieldset>
                <hr />
                <div id="chartContainer" style="height: 300px; width: 100%;"></div>
            </div>
        </div>
    @endif
</div>
<hr />

</body>
</html>
