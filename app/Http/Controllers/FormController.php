<?php

namespace app\Http\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Mail;


class FormController extends Controller
{
    public function showForm()
    {
        return view('index');
    }

    public function submitForm(Request $request)
    {
        //run validations
        $this->validate($request, [
            'company' => 'required',
            'startDate' => 'required|date_format:Y-m-d|before:endDate',
            'endDate' => 'required|date_format:Y-m-d|after:startDate|before:today',
            'email' => 'required|email'
        ]);

        //get the data from Google Finance API
        $httpClient = new Client();
        try {
            $res = $httpClient->get(
                "https://www.google.com/finance/historical?output=csv&q={$request->company}&startdate={$request->startDate}&enddate={$request->endDate}"
            );
        } catch (ClientException $e) {
            return view('index')
                ->withErrors(['company' => ["The requested company wasn't found."]]);
        }

        //parse the data in array, so that we can use in view
        $companyData = $this->parseCSV($res->getBody());

        //this data is used for creating the graph in the UI
        $graphData = $this->parseGraphData($companyData);

        //also send an email
        $this->sendEmail($request);

        return view('index')
            ->with('companyData', $companyData)
            ->with('graphData', $graphData);
    }

    protected function parseCSV($data)
    {
        $result = explode("\n", $data);
        foreach ($result as $index => $a) {
            $result[$index] = explode(',', $a);
        }

        //remove the empty array that's created at the end
        array_pop($result);
        return $result;
    }

    protected function parseGraphData($data)
    {
        $result = array();

        foreach ($data as $index => $row) {
            //ignore the first line, that contains the headers
            if ($index == 0) continue;

            //we push data in this format: {Date, Open, Close}
            array_push($result, new \App\GraphData($row[0], floatval($row[1]), floatval($row[4])));
        }

        return $result;
    }

    /**
     * Sends an email to the email that the user submitted,
     * with the company name as subject
     * and the dates as body
     *
     * @param Request $r
     */
    private function sendEmail(Request $r)
    {
        Mail::raw("From {$r->startDate} to {$r->endDate}", function ($message) use ($r) {
            $message->subject($r->company);
            $message->to($r->email);
        });
    }

}
