<?php
/**
 * Created by PhpStorm.
 * User: admin_2
 * Date: 13/6/2017
 * Time: 6:30 μμ
 */

namespace App;


class GraphData {
    public $date;
    public $open;
    public $close;

    function __construct($date, $open, $close)
    {
        $this->date = $date;
        $this->open = floatval($open);
        $this->close = floatval($close);
    }
}