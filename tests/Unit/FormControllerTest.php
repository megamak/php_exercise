<?php

namespace Tests\Feature;

use App\GraphData;
use Tests\TestCase;

class FormControllerTest extends TestCase
{
    private $sut;
    private $input = "Hello,World
    1,2,3,4,5
    6,7,8,9,0";
    private $expected = [
        ['Hello', 'World'],
        [1, 2, 3, 4, 5]
    ];

    public function setUp()
    {
        $this->sut = new SubFormController();
    }

    public function testParseCSV_removesLastLine()
    {
        $result = $this->sut->parseCSV($this->input);

        $this->assertNotContains([6, 7, 8, 9, 0], $result);
    }

    public function testParseCSV_splitsFirstLineAccordingToCommas()
    {
        $result = $this->sut->parseCSV($this->input);

        $this->assertEquals($result[0], $this->expected[0]);
    }

    public function testParseCSV_returnsEmptyArrayWhenEmptyIsGiven()
    {
        $result = $this->sut->parseCSV(null);

        $this->assertEquals($result, array());
    }

    public function testParseGraphData_getsFieldsFromPositions1and2and5()
    {
        $data = $this->sut->parseCSV($this->input);
        $graphData = $this->sut->parseGraphData($data);

        $this->assertEquals($graphData, [new GraphData('1', '2', '5')]);
    }

    public function testParseGraphData_returnsEmptyWhenInputIsEmpty()
    {
        $data = $this->sut->parseCSV(null);
        $graphData = $this->sut->parseGraphData($data);

        $this->assertEquals($graphData, array());
    }
}

//This class serves as a fast way to test protected methods
class SubFormController extends \App\Http\Controllers\FormController
{
    public function parseCSV($data)
    {
        return parent::parseCSV($data);
    }

    public function parseGraphData($data)
    {
        return parent::parseGraphData($data);
    }
}
